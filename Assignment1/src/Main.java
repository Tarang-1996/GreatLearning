import java.util.Scanner;

public class Main {
	
	
	Scanner sc = new Scanner(System.in);
	
	

	// function to checkPalindrome

	public void checkPalindrome() {
		
		System.out.println("Please enter the number to be checked as palindrome");
		int num =sc.nextInt();
		
		int num1= num;
		int rev=0;
		
		while(num1>0) {
			
			int rem = num1%10;
			rev=10*rev + rem;
			num1=num1/10;
			
		}
		
		if(num==rev) {
			System.out.println("Given number is palindrome.\n");	
			
		}else {
			
			System.out.println("Given number is not a palindrome.\n");
		}
	}

	// function to printPattern

	public void printPattern() {
		
		System.out.println("Please enter the input for printing pattern.");
		int n = sc.nextInt();
		
		for(int i=1; i<=n; i++) {
			for(int j=n; j>=i; j--) {
				System.out.print("*");
			}
			System.out.println("");
		}

	}

	// function to check no is prime or not

	public void checkPrimeNumber() {
		
		System.out.println("Please enter the number for checking prime");
		
		int num = sc.nextInt();
		
		int half = num/2;
		
		boolean isPrime = true;
		
		if(num==2) {
			System.out.println("Given number is  prime.\n");
			return;
		}
		
		for(int i =2 ; i<half ; i++ ){
			
			if(num%i==0) {
				isPrime=false;
				break;
			}
		}
		
		if(isPrime) {
			System.out.println("Given number is prime.\n");
		}else {
			System.out.println("Given number is not prime.\n");
		}

	}

	// function to print Fibonacci Series

	public void printFibonacciSeries() {

		// initialize the first and second value as 0,1 respectively.

		int first = 0, second = 1;
		
		System.out.println("Please enter the number to print the Fibonacci Series");
		
		int num = sc.nextInt();
		
		System.out.print(first  +" ,");
		System.out.print(second  +" ,");
		
		for(int i=2; i<num; i++) {
			int sum = first + second;
			first = second;
			second = sum;
			
			System.out.print(sum+", ");
		}

	}

//main method which contains switch and do while loop

	public static void main(String[] args) {

		Main obj = new Main();

		int choice;

		Scanner sc = new Scanner(System.in);

		do {

			System.out.println(
							"Enter your choice from below list.\n" +
								"1. Find palindrome of number.\n"  + 
								"2. Print Pattern for a given no.\n" + 
								"3. Check whether the no is a  prime number.\n" + 
								"4. Print Fibonacci series.\n" +
								"--> Enter 0 to Exit.\n"
								);

			System.out.println();

			choice = sc.nextInt();

			switch (choice) {

			case 0:

				choice = 0;

				break;

			case 1: {

				obj.checkPalindrome();

			}

				break;

			case 2: {

				obj.printPattern();

			}

				break;

			case 3: {

				obj.checkPrimeNumber();

			}

				break;

			case 4: {

				obj.printFibonacciSeries();

			}

				break;

			default:

				System.out.println("Invalid choice. Enter a valid no.\n");

			}

		} while (choice != 0);

		System.out.println("Exited Successfully!!!");

//		sc.close();

	}

}
